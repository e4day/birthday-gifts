Canon PowerShot SX260 HS
They say it is during our teens when we have our most outrageous memories and there�s little arguing with that general logic. It is therefore most ideal to buy your teen a quality camera for him or her to capture those memories for the future.

The Canon PowerShot SX260 HS is a beast of a camera, tailored for both advanced photographers and casual photo snappers. With excellent image stabilization and fantastic video and photo quality it is one of the best cameras for its price on the market.

Some people assert that there is little need for cameras these days with our mobile phones more than capable of capturing high quality images and pictures. This is understandable but not entirely true. There is something rather special about owning a camera and this is will be felt and appreciated much by teens. Furthermore the quality a proper camera such as this Canon PowerShot can achieve, cannot be replicated by a mobile phone.